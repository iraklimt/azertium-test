# Simple image loader

The project is a simple Vue SPA that loads images when user does scroll down.

The images are provided by jsonplaceholder API and the AJAX requests are made by Axios library.

The SPA is built using the Vue Cli, which helps us to deploy as well as for building a ready to use project.

To improve the user experience, a loader appears while the images are loaded. Once the images have been loaded, they are displayed with a smooth effect.

The user can remove any image (in de DOM as well in a temporal data array) clicking over them.

The default behaviour for the greed system si displaying 4 items for more than 600px and 2 items for less than 600px devices.




## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```
